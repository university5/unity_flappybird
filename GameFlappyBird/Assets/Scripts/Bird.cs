﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Bird : MonoBehaviour
{
    public float speed = 2;

    public float force = 300;

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * force);
    }

     void OnCollisionEnter2D(Collision2D coll)
    {
         SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}